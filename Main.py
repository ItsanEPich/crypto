import sys, os, string, Messages
from random import randint

rand = randint(0, 100)


def win():
    cls()
    print(Messages.win)
    sys.exit()


def lose():
    cls()
    print(Messages.lose)
    sys.exit()


def cls():
    print("\n\n\n")


def main(line, difficulty):
    line = line.lower()
    newarr = {}
    while True:
        for i in range(0, len(line)):
            if line[i] not in newarr:
                if line[i] in (string.ascii_letters):
                    newarr[line[i]] = chr(((rand + ord(line[i])) % 26) + 97)
                else:
                    newarr[line[i]] = line[i]
        cypher = []
        cherta = []
        for i in range(0, len(line)):
            cypher.append(newarr[line[i]])
            if newarr[line[i]] not in line[i]:
                cherta.append("_")
            else:
                cherta.append(line[i])
        if (line != cypher):
            break
    for i in range(0, len(line)):
        if newarr[line[i]] not in (line[i]):
            while line != cypher:
                if (difficulty == 0):
                    lose()
                print("Encrypted String:", "".join(cypher))
                print("Decrypted String:", "".join(cherta))
                print("What is hidden behind ", newarr[line[i]], ":")
                guess = input()
                if guess not in ("\n") and guess in line[i]:
                    cls()
                    print("You are right!! You still have", difficulty, "tries remaining")
                    newarr[line[i]] = line[i]
                    for i in range(0, len(line)):
                        if newarr[line[i]] not in line[i]:
                            cherta[i] = "_"
                        else:
                            cherta[i] = newarr[line[i]]
                            cypher[i] = newarr[line[i]]
                    break
                else:
                    cls()
                    difficulty = difficulty - 1
                    print("You are wrong!! And you have", difficulty, "tries remaining")
    win()
    return


def singleplayer():
    difficulty = input("Please, enter difficulty: easy, normal, hard or hardcore: ")
    cls()
    if difficulty in ("easy"):
        difficulty = 10
        print("You've got 10 tries!")
    elif difficulty in ("normal"):
        difficulty = 7
        print("You've got 7 tries!")
    elif difficulty in ("hard"):
        difficulty = 4
        print("You've got 4 tries!")
    elif difficulty in ("hardcore"):
        difficulty = 1
        print("You've got only 1 try!")
    else:
        print("Since you are trying to be a smartass, you get hardcore mode!")
        difficulty = 1
        print("You've got only 1 try!")
    f = open('Quotes', 'r')
    # Line Counter
    num_lines = sum(1 for numline in open('Quotes'))
    randqu = randint(1, num_lines)
    with open('Quotes') as f:
        for i, line in enumerate(f, 1):
            if i == randqu:
                break;
    f.close()
    main(line, difficulty)
    return


def multiplayer():
    line = input("Please, enter a sentence: ")
    difficulty = 0
    while True:
        try:
            difficulty = int(input("Please, enter a number, greater than 0: "))
        except ValueError:
            print("Could you at least give me an actual number?")
            continue
        if difficulty > 0:
            break
    cls()
    main(line, difficulty)
    return


def host():
    line = input("Please, enter a sentence: ")
    difficulty = 0
    while True:
        try:
            difficulty = int(input("Please, enter a number, greater than 0: "))
        except ValueError:
            print("Could you at least give me an actual number?")
            continue
        if difficulty > 0:
            break
    file = open('gift.cqp', 'w+')
    file.write(line)
    file.write("\n")
    file.write(str(difficulty))


def join():
    f = open('gift.cqp', 'rw')
    with open('gift.cqp') as f:
        for i, line in enumerate(f, 1):
            if i == 1:
                break;
    with open('gift.cqp') as f:
        for i, difficulty in enumerate(f, 1):
            if i == 2:
                break;
    main(line, int(difficulty))


def online():
    hostornot = input('Choose whether you are hosting(0) or joining(1) a game: ')
    if hostornot in ['host', '0']:
        host()
    elif hostornot in ['join', '1']:
        join()


def welcome():
    cls()
    mode = input('Choose singleplayer(0) or multiplayer(1) or online(2):\n')
    if mode in ['singleplayer', 's', '0']:
        singleplayer()
    elif mode in ['multiplayer', 'm', '1']:
        multiplayer()
    elif mode in ['online', '2', '0']:
        online()
    else:
        print("Invalid command. No crypto-quote-puzzle for you!")
    return


welcome()
sys.exit()
